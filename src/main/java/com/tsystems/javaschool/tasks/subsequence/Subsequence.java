package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int count = 0;
        int fall = 0;
        for (Object s : x) {
            for (int j = fall; j < y.size(); j++) {
                if (s.equals(y.get(j))) {
                    count++;
                    fall = j;
                    break;
                }
            }
        }
        return count == x.size();
    }
}
