package com.tsystems.javaschool.tasks.calculator;

public class CalculatorException extends RuntimeException {

    public CalculatorException(String errStr) {
        super(errStr);
    }
}
