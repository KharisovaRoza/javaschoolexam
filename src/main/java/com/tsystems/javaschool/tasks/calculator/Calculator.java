package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        double result;

        exp = statement;
        IndexAtNow = 0;
        try {
            getToken();

            if (token.equals(EndOfFunction))
                handleErr(NOEXP);   //  Нет выражения

            //  Анализ и вычисление выражения
            result = evalExp2();

            if (!token.equals(EndOfFunction))
                handleErr(SYNTAXERROR);
            return String.valueOf(result);
        } catch (CalculatorException e) {
            return null;
        }
    }


    private final int NONE = 0;         //  FAIL
    private final int DELIMITER = 1;    //  Разделитель(+-*/^=, ")", "(" )
    private final int VARIABLE = 2;     //  Переменная
    private final int NUMBER = 3;       //  Число

    private final int SYNTAXERROR = 0;  //  Синтаксическая ошибка (10 + 5 6 / 1)
    private final int UNBALPARENS = 1;  //  Несовпадение количества открытых и закрытых скобок
    private final int NOEXP = 2;        //  Отсутствует выражение при запуске анализатора
    private final int DIVBYZERO = 3;    //  Ошибка деления на ноль

    private final String EndOfFunction = "\0";

    private String exp;     //  Ссылка на строку с выражением
    private int IndexAtNow;     //  Текущий индекс в выражении
    private String token;   //  Сохранение текущей лексемы
    private int tokType;    //  Сохранение типа лексемы


    //  Получить следующую лексему
    private void getToken() {
        tokType = NONE;
        token = "";

        //  Проверка на окончание выражения
        if (IndexAtNow == exp.length()) {
            token = EndOfFunction;
            return;
        }
        //  Проверка на пробелы, если есть пробел - игнорируем его.
        while (IndexAtNow < exp.length() && Character.isWhitespace(exp.charAt(IndexAtNow)))
            ++IndexAtNow;
        //  Проверка на окончание выражения
        if (IndexAtNow == exp.length()) {
            token = EndOfFunction;
            return;
        }
        if (isDelim(exp.charAt(IndexAtNow))) {
            token += exp.charAt(IndexAtNow);
            IndexAtNow++;
            tokType = DELIMITER;
        } else if (Character.isLetter(exp.charAt(IndexAtNow))) {
            while (!isDelim(exp.charAt(IndexAtNow))) {
                token += exp.charAt(IndexAtNow);
                IndexAtNow++;
                if (IndexAtNow >= exp.length())
                    break;
            }
            tokType = VARIABLE;
        } else if (Character.isDigit(exp.charAt(IndexAtNow))) {
            while (!isDelim(exp.charAt(IndexAtNow))) {
                token += exp.charAt(IndexAtNow);
                IndexAtNow++;
                if (IndexAtNow >= exp.length())
                    break;
            }
            tokType = NUMBER;
        } else {
            token = EndOfFunction;
        }
    }

    private boolean isDelim(char charAt) {
        return (" +-/*%^=()".indexOf(charAt)) != -1;
    }

    //  Сложить или вычислить два терма
    private double evalExp2() {

        char op;
        double result;
        double partialResult;
        result = evalExp3();
        while ((op = token.charAt(0)) == '+' ||
                op == '-') {
            getToken();
            partialResult = evalExp3();
            switch (op) {
                case '-':
                    result -= partialResult;
                    break;
                case '+':
                    result += partialResult;
                    break;
            }
        }
        return result;
    }

    //  Умножить или разделить два фактора
    private double evalExp3() {

        char op;
        double result;
        double partialResult;

        result = evalExp5();
        while ((op = token.charAt(0)) == '*' ||
                op == '/' | op == '%') {
            getToken();
            partialResult = evalExp5();
            switch (op) {
                case '*':
                    result *= partialResult;
                    break;
                case '/':
                    if (partialResult == 0.0)
                        handleErr(DIVBYZERO);
                    result /= partialResult;
                    break;
            }
        }
        return result;
    }

    //  Определить унарные + или -
    private double evalExp5() {
        double result;

        String op;
        op = " ";

        if ((tokType == DELIMITER) && token.equals("+") ||
                token.equals("-")) {
            op = token;
            getToken();
        }
        result = evalExp6();
        if (op.equals("-"))
            result = -result;
        return result;
    }

    //  Обработать выражение в скобках
    private double evalExp6() {
        double result;

        if (token.equals("(")) {
            getToken();
            result = evalExp2();
            if (!token.equals(")"))
                handleErr(UNBALPARENS);
            getToken();
        } else
            result = atom();
        return result;
    }

    //  Получить значение числа
    private double atom() {

        double result = 0.0;
        switch (tokType) {
            case NUMBER:
                try {
                    result = Double.parseDouble(token);
                } catch (NumberFormatException exc) {
                    handleErr(SYNTAXERROR);
                }
                getToken();

                break;
            default:
                handleErr(SYNTAXERROR);
                break;
        }
        return result;
    }

    //  Кинуть ошибку
    private void handleErr(int nOEXP2) {

        String[] err = {
                "Syntax error",
                "Unbalanced Parentheses",
                "No Expression Present",
                "Division by zero"
        };
        throw new CalculatorException(err[nOEXP2]);
    }

}
