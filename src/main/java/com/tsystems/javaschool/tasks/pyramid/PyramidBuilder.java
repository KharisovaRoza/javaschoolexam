package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import static java.lang.Math.sqrt;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        Collections.sort(inputNumbers);
        checkPyramidMayBeBuild(inputNumbers);
        int n = getNumberOfLevels(inputNumbers.size());
        int[][] result = new int[n][2 * n - 1];
        Iterator<Integer> iterator = inputNumbers.iterator();
        for (int i = 0; i < n; i++) {
            int j = 0;
            for (; j < n - i - 1; j++) {
                result[i][j] = 0;
            }
            for (; j < n + i; j++) {
                if (j == 0 || result[i][j - 1] == 0) {
                    result[i][j] = iterator.next();
                } else {
                    result[i][j] = 0;
                }
            }
            for (; j < result[i].length; j++) {
                result[i][j] = 0;
            }
        }
        return result;
    }

    private void checkPyramidMayBeBuild(List<Integer> array) {
        if (array.contains(0)) {
            throw new CannotBuildPyramidException("В массиве есть нули!");
        }

        if (array.size() != new HashSet<>(array).size()) {
            throw new CannotBuildPyramidException("Есть повторяющиеся элементы!");
        }
    }

    private int getNumberOfLevels(int elementsNumber) {
        double n = sqrt(0.25 + 2 * elementsNumber) - 0.5;
        if (n != Math.floor(n)) {
            throw new CannotBuildPyramidException("Неверное число элементов!");
        }
        return Math.toIntExact(Math.round(n));
    }


}
